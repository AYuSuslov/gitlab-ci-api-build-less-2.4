## Описание задания

1. Сделать Форк репозитория https://gitlab.com/Sibiryakanton/juneway_less_1
2. Создать ветку Dev
3. Сделать сборку имеджей из 2х веток
Dev Master (имеджи должны иметь тег :app_dev sha app_prod sha
4. Написать деплой (запуск) приложения только в своих ветках.
Пуш в деве - собирается \ запускается долько Дев.
5. В композе порты для нжингса должны быть ввиде переменной Прод - 8000 Дев 8001
(какой тип переменной использовать, решать вам)

Задача *

Сделайте реджистори юрл с помощью переменных.

## Ответ

Ссылка на репозиторий проекта в Gitlab: https://gitlab.com/AYuSuslov/pipelines-and-registries-gitlab-ci-less-2.1
В проекте две ветки - *master* и *Dev*. Ветки по сути отличаются только содержимым файла "manage.py". В обоих ветках запушены рабочие файлы *docker-compose.yml* и *.gitlab-ci.yml*. В ветке *master* собирается образ с версией :app_prod_sha и он же запускается в compose, в ветке *Dev* собирается образ :app_dev_sha и запускается в compose.
Переменную для порта nginx определяю непосредственно в *gitlab-ci.yml*. В последних версиях пайплайнов используется переменная для registry url.